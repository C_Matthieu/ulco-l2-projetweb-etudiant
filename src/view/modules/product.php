<?php $p=$params["produit"];
    $comment=isset($params["comment"][0]);
    $logged=isset($_SESSION['firstname']);

?>

<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $p["image"] ?>">
            <div class="product-miniatures">
                <div>
                    <img src="/public/images/<?= $p["image"] ?>">
                </div>
                <div>
                    <img src="/public/images/<?= $p["image_alt1"] ?>">
                </div>
                <div>
                    <img src="/public/images/<?= $p["image_alt2"] ?>">
                </div>
                <div>
                    <img src="/public/images/<?= $p["image_alt3"] ?>">
                </div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category">
                <?= $p["category"] ?>
            </p>
            <h1><?= $p["name"] ?></h1>
            <p class="product-price">
                <?= $p["price"] ?>€
            </p>
            <form method="post" action="/cart/add">
                <button type="button">-</button>
                <button type="button" >1</button>
                <button type="button">+</button>
                <input id="quantity" name="quantity" value="1"  style="display: none"/>
                <input name="product" value="<?php echo $p["id"]?>" style="display: none"/>
                <input type="submit" value="Ajouter au panier" />


            </form>

        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2>
                Spécialités
            </h2>
            <?= $p["spec"] ?>
        </div>
        <div class="product-comments">
            <h2>
                Avis
            </h2>
            <?php if ($comment):?>

                <?php for ($i=0;$i<count($params["comment"]);$i++){ ?>
                    <div class="product-comment">
                        <p class="product-comment-author">
                            <?php echo $params["comment"][$i]['firstname']. " ". $params["comment"][$i]['lastname'] ;?>
                        <p>
                            <?php echo $params["comment"][$i]['content'];?>
                        </p>
                    </div>
                <?php }?>
            <?php else: ?>

                <p>
                    Il n'y a pas d'avis pour ce produit
                </p>
            <?php endif ;?>

            <?php if ($logged){?>

                <form method="post" action="/store/<?= $p["id"] ?>/postComment">
                    <input type="text" name="comment" placeholder="Rédiger un commentaire" />
                </form>
            <?php } ?>

        </div>
    </div>

</div>
<script src="/public/scripts/product.js"></script>
