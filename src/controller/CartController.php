<?php
namespace controller;

class cartController{

    public function cart(): void
    {
        // Communications avec la base de données


        // Variables à transmettre à la vue
        $params = array(
            "title" => "Cart",
            "module" => "cart.php"
        );
        \view\Template::render($params);
    }

    public function add(): void{

        $quantity=$_POST["quantity"];
        $product_id=$_POST["product"];
        $produit= \model\CartModel::infoProduct($product_id);
        $cart=array("quantity"=> $quantity,
                    "id"=>$produit['id'],
                    "name"=>$produit['name'],
            "price"=>$produit['price'],
            "image"=>$produit['image'],
            "category"=>$produit['category']);

        if(!isset( $_SESSION["cart"])){
            $_SESSION["cart"][$product_id]=$cart;
        }else{
            if(array_key_exists($product_id,$_SESSION["cart"])){
                $_SESSION["cart"][$product_id]["quantity"]=$_SESSION["cart"][$product_id]["quantity"]+$cart["quantity"];
                if ( $_SESSION["cart"][$product_id]["quantity"]>5)  $_SESSION["cart"][$product_id]["quantity"]=5;
            }else{

                $_SESSION["cart"][$product_id]=$cart;
            }
        }

        header("Location:/cart");
        exit();



    }

}
