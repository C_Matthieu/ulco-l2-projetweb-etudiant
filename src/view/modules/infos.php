
<?php  $state=isset($_GET['status'])   ?>
<?php     ?>
<?php  if($state){?>
    <?php   if($_GET['status']=="update_success") {?>
        <div class="box info" style="margin: 32px">
            Tes informations personnelles ont bien été mis à jour !
        </div>
    <?php }elseif ($_GET['status']=="update_fail"){?>
        <div class="box error" style="margin: 32px">
            Erreur lors de la mise à jour des informations personnelles. Veuillez réessayez !
        </div>

    <?php } ?>
<?php }   ?>
<div id="account">
    <form class="" method="post" action="/account/update">

        <h2>Informations du compte</h2>
        <h3>Information personnelles </h3>

        <p>Prénom</p>
        <input type="text" name="firstname" placeholder="<?php echo $_SESSION['firstname']?>" />

        <p>Nom</p>
        <input type="text" name="lastname" placeholder="<?php echo $_SESSION['lastname']?>" />

        <p>Adresse mail</p>
        <input type="text" name="mail" placeholder="<?php echo $_SESSION['mail']?>" />
        <input type="submit" value="Modifier mes informations" />

    </form>
</div>

