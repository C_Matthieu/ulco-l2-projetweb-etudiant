<?php
namespace model;
class CommentModel{
    static function insertComment($content,$id,$id_account){
        $db = \model\Model::connect();

        // Requête SQL
        $sql = 'INSERT INTO `comment`(`content`, `date`, `id_product`, `id_account`) value ("'.htmlspecialchars($content).'",NOW(),'.$id.','.$id_account.')';
        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();


    }
    static function listComment($id): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT content , id_product , a.firstname as firstname , a.lastname as lastname  FROM comment as c
                    inner join product as p on p.id =c.id_product
                    inner join account as a on a.id =c.id_account
                        where id_product=".$id;


        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }


}