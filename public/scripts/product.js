
let product_min = document.getElementsByClassName("product-miniatures");
let div =product_min[0].getElementsByTagName("div")
let product_max = document.getElementsByClassName("product-images");
let img_max=product_max[0].getElementsByTagName("img");
let btn= document.getElementsByTagName("button");
let product_infos=document.getElementsByClassName("product-infos")
let error=document.getElementsByClassName("box error");
let input=document.getElementById('quantity')


function affichage(){
    for(let current of div){
        current.addEventListener('click',function (){
            let img =current.getElementsByTagName("img");
            img_max[0].setAttribute("src",img[0].src)
        })
    }
}

function add_div_error(){
    let new_div=document.createElement("div")
    new_div.innerHTML="Quantité maximale autorisée !";
    new_div.classList.add("box");
    new_div.classList.add("error");
    product_infos[0].appendChild(new_div);

}
function suprim_div_error(){
    let div=product_infos[0].getElementsByClassName("box error");
    div[0].remove();
}
function quantite(){
    btn[0].addEventListener('click',function (){
        let valeur=parseInt(btn[1].innerHTML);
        if(valeur!=1){
            if(valeur==5){
                suprim_div_error();
            }
            valeur--;
            btn[1].innerHTML=valeur;
            input.value=valeur;
            console.log(input.value);
        }
    })
    btn[2].addEventListener('click',function (){
        let valeur=parseInt(btn[1].innerHTML);
        if(valeur!=5){
            if (valeur==4){
                add_div_error();
            }
            valeur++;
            btn[1].innerHTML=valeur;
            input.value=valeur;
            console.log(input.value);
        }
    })

}
document.addEventListener("DOMContentLoaded", function (){
    affichage();
    quantite();



})