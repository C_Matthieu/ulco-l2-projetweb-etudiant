<?php

/** Initialisation de l'autoloading et du router ******************************/
session_start();
require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"
$router->get('/', 'controller\IndexController@index');
$router->get('/store', 'controller\StoreController@store');
$router->get('/store/{:num}', 'controller\StoreController@product');
$router->get('/account', 'controller\AccountController@account');
$router->get('/account/logout', 'controller\AccountController@logout');
$router->get('/cart', 'controller\CartController@cart');
if(isset($_SESSION['id'])){
    $router->get('/account/infos', 'controller\AccountController@infos');

}
//POST
$router->post('/account/login', 'controller\AccountController@login');
$router->post('/account/signin', 'controller\AccountController@signin');
$router->post('/store/{:num}/postComment', 'controller\CommentController@postComment');
$router->post('/store', 'controller\StoreController@search');
$router->post('/account/update', 'controller\AccountController@update');
$router->post('/cart/add', 'controller\CartController@add');


// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
