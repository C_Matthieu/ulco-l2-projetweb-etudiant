<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $produits = \model\StoreModel::listProducts();

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
      "produits" => $produits
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }
    public function product($id): void
    {
        if($id>9){
            header("Location:/store");
            exit();
        }
        $produit= \model\StoreModel::infoProduct($id);
        $comment=\model\CommentModel::listComment($id);
        $params = array(
            "title" => "Product ".$id ,
            "module" => "product.php",
            "produit" => $produit,
            "comment" => $comment
        );
        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }
    public function search(): void
    {
        $categories = \model\StoreModel::listCategories();

        $search=$_POST['search'];
        $category=isset($_POST['category'])?$_POST['category']:null;
        $order=isset($_POST['order'])?$_POST['order']:null;

        if(!isset($category))    {
            $i=0;
            foreach ($categories as $c){
                $category[$i]=$c['name'];
                $i++;
            }
        }

        $produits = \model\StoreModel::searchProducts($search,$category,$order);

        // Variables à transmettre à la vue
        $params = array(
            "title" => "Store",
            "module" => "store.php",
            "categories" => $categories,
            "produits" => $produits
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);



    }

}