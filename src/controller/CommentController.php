<?php
namespace controller;

class CommentController{

    public function postComment($id) :void{
        $content=$_POST['comment'];
        $id_account=$_SESSION['id'];
        \model\CommentModel::insertComment($content,$id,$id_account);

        header("Location:/store/".$id);
        exit();
    }



}