<?php $logged=isset($_SESSION['firstname']);?>

<nav>
    <img src="/public/images/logo.jpeg" />
    <a href="/" >Accueil</a>
    <a href="/store" >Boutique</a>
    <?php if($logged){?>
        <a class="account" href="/account/infos" >
            <img src="/public/images/avatar.png" />
            <?php echo $_SESSION['firstname'];  echo " ". $_SESSION['lastname'] ?>
        </a>
        <a href="/cart" >
            PANIER
        </a>
        <a href="/account/logout" >
            DÉCONNEXION
        </a>
    <?php }else{?>

        <a class="account" href="/account" >
            <img src="/public/images/avatar.png" />
            Compte
        </a>


    <?php }?>
</nav>