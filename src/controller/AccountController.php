<?php
namespace controller;

use model\AccountModel;

class AccountController{

    public function account() :void{
        $params = array(
            "title" => "Account",
            "module" => "account.php"
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }
    public function signin () :void{


        $firstname=$_POST['userfirstname'];
        $lastname=$_POST['userlastname'];
        $mail=$_POST['usermail'];
        $password=$_POST['userpass'];
        $test=\model\AccountModel::signin($firstname, $lastname, $mail, $password);
        if($test==true){
            header("Location:/account?status=signin_success");
            exit();
        }else{
            header("Location:/account?status=signin_fail");
            exit();
        }


    }
    public function login () :void
    {
        $mail=$_POST['usermail'];
        $password=$_POST['userpass'];

        $user=\model\AccountModel::login($mail,$password);

        if($user==false){
            header("Location: /account?status=login_fail");
            exit();

        }else{
            $_SESSION['id']=$user['id'];
            $_SESSION['firstname']=$user['firstname'];
            $_SESSION['lastname']=$user['lastname'];
            $_SESSION['mail']=$user['mail'];
            $_SESSION['password']=$user['password'];

            header("Location:/store");
            exit();

        }

    }

    public function logout():void {

        session_destroy() ;
        header("Location:/account?status=logout");
        exit();

    }

    public function infos():void {
        $params = array(
            "title" => "Infos",
            "module" => "infos.php"
        );

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);

    }
    public function update():void {
        $firstname=$_POST['firstname'];
        $lastname=$_POST['lastname'];
        $mail=$_POST['mail'];
        $update=\model\AccountModel::update($firstname,$lastname,$mail);
        if($update){
            $_SESSION['firstname']=htmlspecialchars($firstname);
            $_SESSION['lastname']=htmlspecialchars($lastname);
            $_SESSION['mail']=htmlspecialchars($mail);
            header("Location:/account/infos?status=update_success");
            exit();
        }
        header("Location:/account/infos?status=update_fail");
        exit();

    }

    }