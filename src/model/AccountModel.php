<?php
namespace model;

use PDO;

class AccountModel{
    static function check($firstname, $lastname, $mail, $password) :bool{
        $verif=true;

        if(strlen($firstname)<2) $verif=false;
        if(strlen($lastname)<2) $verif=false;
        if(!filter_var($mail,FILTER_VALIDATE_EMAIL)) $verif=false;

        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = 'SELECT mail FROM account where mail= "'.$mail.'"';

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();
        $Array_mail=$req->fetch();
        if($Array_mail!=null) $verif=false;

        if(strlen($password)<6) $verif=false;

        return $verif;

    }
    static function signin($firstname, $lastname, $mail, $password) :bool{

        if(AccountModel::check($firstname, $lastname, $mail, $password)){
            $db = \model\Model::connect();

            // Requête SQL
            $sql = 'INSERT INTO `account`(`firstname`, `lastname`, `mail`, `password`) value ("'.htmlspecialchars($firstname).'","'.htmlspecialchars($lastname).'","'.htmlspecialchars($mail).'","'.htmlspecialchars($password).'")';
            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute();

            return true;
        }
        return false;

    }
    static function login($mail,$password){
        $db = \model\Model::connect();

        // Requête SQL
        $sql = 'SELECT * from `account` where mail="'.htmlspecialchars($mail).'" and password="'.htmlspecialchars($password).'"';
        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        return $req->fetch();



    }
    static function update($firstname, $lastname, $mail):bool{
        if(AccountModel::check($firstname, $lastname, $mail, $_SESSION['password'])){
            $db = \model\Model::connect();

            // Requête SQL
            $sql = 'UPDATE account set firstname="'.htmlspecialchars($firstname).'", lastname="'.htmlspecialchars($lastname).'", mail="'.htmlspecialchars($mail).'" where id='.$_SESSION['id'];
            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute();

            return true;
        }
        return false;



    }
}