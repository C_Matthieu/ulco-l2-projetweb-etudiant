<?php
$null=isset($_SESSION["cart"]);
?>
<div id="cart">

    <h1> Panier </h1>
    <?php if($null):?>
    <div class="products">

        <?php foreach ($_SESSION["cart"] as $product) { ?>
            <div class="card">
                <p class="card-image">
                    <img src="/public/images/<?= $product["image"] ?>" />
                </p>
                <div class="card-category_title">
                    <p class="card-category">
                        <?= $product["category"] ?>
                    </p>
                    <p class="card-title">
                        <?= $product["name"] ?>

                    </p>

                </div>

                <div class="card-quantity_div">
                    <p class="card-quantity">
                        Quantité :
                    </p>
                    <div class="card-quantity">
                        <button id="btn_minus" type="button">-</button>
                        <button class="btn_quantity" type="button" ><?= $product["quantity"] ?></button>
                        <button id="btn_plus" type="button">+</button>
                    </div>


                </div>
                <div class="card-price_div">
                    <p class="card-title_price">
                       Prix unitaire :
                    </p>
                    <p class="card-price"><span ><?= $product["price"] ?></span>€</p>

                </div>


            </div>
        <?php } ?>
        <div class="total">
            <p class="card-image"></p>
            <div class="card-category_title"></div>
            <div class="card-quantity_div"></div>
            <div class="total_price" >
                <p class="card-title_price">
                    Prix total du panier :
                </p>
                <p class="card-total_price">

                </p>
            </div>

        </div>

        <form class="card-from">
            <input type="submit" value="Procéder au paiment" />
        </form>
    </div>


    <?php else :?>
        <div class="products" style="margin: 0px 32px;">

            Ton panier est vide

        </div>
    <?php endif;?>
</div>

<script>

    document.addEventListener('DOMContentLoaded', function () {
        let price=document.getElementsByTagName("span");
        let quantity=document.getElementsByClassName("btn_quantity");
        let ptotal=0;
        console.log(quantity[0].innerHTML);
        for (let i = 0; i < price.length; i++) {

            ptotal += parseInt(price[i].innerHTML) * parseInt(quantity[i].innerHTML);

        }
        let div = document.getElementsByClassName("card-total_price");
        div[0].innerHTML = ptotal+"€";
    })
</script>
