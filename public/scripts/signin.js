let from=document.getElementsByClassName('account-signin');
let input=from[0].getElementsByTagName('input');
let p=from[0].getElementsByTagName('p');


function nom(){
    //NOM
    input[0].addEventListener('focusout',function () {
        let text=input[0].value;
        console.log(text.length);
        input[0].classList.remove('valid','invalid');
        p[0].classList.remove('valid','invalid');
        if(text.length>=2){
            input[0].classList.add('valid');
            p[0].classList.add('valid');


        }else{
            input[0].classList.add('invalid');
            p[0].classList.add('invalid');

        }
        envoi();
    })


}
function prenom(){
    input[1].addEventListener('focusout',function () {
        //PRENOM
        let text=input[1].value;
        console.log(text.length);
        input[1].classList.remove('valid','invalid');
        p[1].classList.remove('valid','invalid');
        if(text.length>=2){
            input[1].classList.add('valid');
            p[1].classList.add('valid');

        }else{
            input[1].classList.add('invalid');
            p[1].classList.add('invalid');

        }
        envoi();
    })



}

function email(){
    //MAIL
    let pattern = /^[^\W][a-zA-Z0-9\-\._]+[^\W]@[^\W][a-zA-Z0-9\-\._]+[^\W]\.[a-zA-Z]{2,4}$/;
    input[2].addEventListener('focusout',function () {
        let text=input[2].value;
        input[2].classList.remove('valid','invalid');
        p[2].classList.remove('valid','invalid');

        if(pattern.test(text)){
            input[2].classList.add('valid');
            p[2].classList.add('valid');

        }else{
            input[2].classList.add('invalid');
            p[2].classList.add('invalid');

        }
        envoi();

    })

}

function mdp(){
    //MOT DE PASSE
    let mdp = /^(?=.*[A-Za-z])(?=.*\d)[a-zA-Z\d]{6,}$/;
    input[3].addEventListener('focusout',function () {
        let text=input[3].value;
        input[3].classList.remove('valid','invalid');
        p[3].classList.remove('valid','invalid');

        if(mdp.test(text)){
            input[3].classList.add('valid');
            p[3].classList.add('valid');

        }else{
            input[3].classList.add('invalid');
            p[3].classList.add('invalid');

        }
        envoi();
    })


}



function rep_mdp() {
    let mdp = /^(?=.*[A-Za-z])(?=.*\d)[a-zA-Z\d]{6,}$/;

    input[4].addEventListener('focusout', function () {
        let text = input[4].value;
        input[4].classList.remove('valid', 'invalid');
        p[4].classList.remove('valid', 'invalid');

        if (mdp.test(text) && input[3].value === input[4].value) {

            input[4].classList.add('valid');
            p[4].classList.add('valid');

        } else {
            input[4].classList.add('invalid');
            p[4].classList.add('invalid');

        }
        envoi();
    })

}

function envoi(){
    let verif=true;

    if(!input[0].classList.contains('valid')){
        verif=false;
    }

    if(!input[1].classList.contains('valid')){
        verif=false;
    }

    if(!input[2].classList.contains('valid')){
        verif=false;
    }

    if(!input[3].classList.contains('valid')){
        verif=false;
    }

    if(!input[4].classList.contains('valid')){
        verif=false;
    }

    if(verif==false){
        input[5].disabled=true;
        console.log("input[5].disabled=true;")
    }else{
        input[5].disabled=false;
        console.log("input[5].disabled=false;")
    }
}

document.addEventListener("DOMContentLoaded", function (){
    nom();
    prenom();
    email();
    mdp();
    rep_mdp();
    envoi();


})