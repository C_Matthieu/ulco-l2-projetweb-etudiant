<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }
    static function listProducts(): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT product.id, product.name, price, image ,category.name as category FROM product INNER JOIN category ON product.category =category.id";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }
    static function infoProduct(int $id): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT product.id, product.name, price, image, image_alt1,image_alt2,image_alt3 ,spec,category.name as category FROM product INNER JOIN category ON product.category =category.id where product.id=".$id;

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetch();
    }
    static function searchProducts($search,$category,$order): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = 'SELECT p.id , p.name, p.price , p.image , c.name as category from product as p INNER JOIN category as c on p.category=c.id where  p.name LIKE "%'.htmlspecialchars($search).'%"and (';
        $i=1;
        foreach ($category as $c){
                $sql=$sql.'c.name="'.$c.'" ';
                if(count($category)>$i){
                    $sql=$sql.'or ';
                }
                $i++;
        }
        $sql=$sql.')';

        if (isset($order)){
            $sql=$sql.'ORDER by p.price '.$order;
        }

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }

}