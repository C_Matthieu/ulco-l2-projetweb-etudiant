<?php
namespace model;

class CartModel{

    static function infoProduct(int $id): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT product.id, product.name, price, image,category.name as category FROM product INNER JOIN category ON product.category =category.id where product.id=".$id;

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetch();
    }




}